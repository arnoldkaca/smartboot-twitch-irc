from db.database import DB_Connect
from time import sleep
from threading import Thread
from controllers.User import User
import socket
import datetime
import requests
import threading

class App:
    username            =   "smartboot"
    channel_name        =   "smartboot"
    password            =   "oauth:64rskshenfmbaib2nrlo4q6yaizk1b"
    irc                 =   None
    host                =   "irc.twitch.tv"
    port                =   6667
    users               =   {}          #user id            (all)
    user                =   []          #user username      (all)
    db                  =   None
    events              =   []
    live_settings       =   {}
    leaderboard         =   {}
    chatters            =   {}

    host_list           =   []

    def __init__(self):
        self.db = DB_Connect()
        self.user = User()
        

    def connect(self):
        self.irc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.irc.connect((self.host, self.port))
        self.irc.send(("PASS " + self.password + "\n").encode('utf-8'))
        self.irc.send(("NICK " + self.username + "\n").encode('utf-8'))
    
    def join_channel(self, channel_name):
        self.irc.send(("JOIN #" + channel_name + "\n").encode('utf-8'))

    def updateUsers(self):
        cursor = self.db.get().cursor()
        cursor.execute("SELECT username.username, user.id FROM username inner join user on user.id=username.user where user.active=1 and username.active=1")
        result = cursor.fetchall()
        for x in result:
            self.users[x[0]] = x[1]
        self.db.close()
    
    def existsInLurkList(self, channel_name, user_id):
        cursor = self.db.get().cursor()
        cursor.execute("select channel_name from lurk_list where user={uid} and channel_name='{chnm}'".format(uid=user_id, chnm=channel_name))
        result = cursor.fetchall()
        self.db.close()
        if(len(result) > 0):
            return True
        return False
    
    def channel_info(self, channel_name):
        url = "https://api.twitch.tv/helix/users?login=" + channel_name
        headers = {'Authorization' : 'Bearer ' + self.user.smartboot['token']}
        r = requests.get(url, headers=headers).json()
        return r

    def is_following(self, follower, followed):
        url = "https://api.twitch.tv/helix/users/follows?from_id="+self.channel_info(follower)['data'][0]['id']+"&to_id="+self.channel_info(followed)['data'][0]['id']
        headers = {'Authorization' : 'Bearer ' + self.user.smartboot['token']}
        r = requests.get(url, headers=headers).json()
        if(r['total']) == 0:
            return False
        return True
    
    def addToLurkList(self, channel_name, user_id):
        if not self.existsInLurkList(channel_name, user_id) :
            cursor = self.db.get().cursor()
            cursor.execute("insert into lurk_list (channel_name, user) values ('{chnm}', {uid})".format(uid=user_id, chnm=channel_name))
            self.db.commit()
            self.db.close()

    def existsInFollowList(self, channel_name, user_id):
        cursor = self.db.get().cursor()
        cursor.execute("select channel_name from follow_list where user={uid} and channel_name='{chnm}'".format(uid=user_id, chnm=channel_name))
        result = cursor.fetchall()
        self.db.close()
        if(len(result) > 0):
            return True
        return False
    
    def AddToFollowList(self, channel_name, user_id):
        if not self.existsInFollowList(channel_name, user_id) :
            cursor = self.db.get().cursor()
            cursor.execute("insert into follow_list (channel_name, user) values ('{chnm}', {uid})".format( chnm=channel_name, uid=user_id))
            self.db.commit()
            self.db.close()

    def sort_dictionary(self, dictionary):
        temp = {}
        for key, value in sorted(dictionary.items(), key=lambda item: item[1]):
            temp[key] = value
        return temp

    def update_Leaderboard(self):
        while True:

            # sort chatting (by amount)
            self.chatters = self.sort_dictionary(self.chatters)
            
            for a in self.leaderboard:
                
                # adding chat bonus
                if a in self.chatters:
                    self.leaderboard[a] += self.chatters[a]
                # adding subscribe bonus
                
            

            
            sleep(1.5)
    

    def pong(self):
        self.irc.send(('PONG :tmi.twitch.tv\n').encode('utf-8'))
        print("ponging", )
    
    def send_message(self, channel_name, message):
        self.irc.send(("PRIVMSG #"+channel_name+" :"+message+" \n").encode('utf-8')) 

    def proccess_server_message(self, response):

        if 'PING :tmi.twitch.tv' in response:
            self.irc.send(("PONG :tmi.twitch.tv\n").encode('utf-8'))
            print("ponging", flush=True)

        if 'PRIVMSG' in response:
            
            message = response.split(" ",3)
            sender = message[0].strip(":").split('!', 1)[0]
            receiver = message[2].strip('#')
            message_text = message[3].strip(":\r\n").split(" ")

            if sender not in self.chatters:
                self.chatters[sender] = 0
            self.chatters[sender] = self.chatters[sender] + 1


            #commands by users in chat
            for x in self.user.custom_commands:
                if receiver in x:

                    #host command
                    if(x[1]) in message_text:
                        self.send_message(sender, "@" + sender + " has entered the host raffle! Leaderboard updates every 90.")
                        if sender not in self.leaderboard:
                            self.leaderboard[sender] = 0
                        self.leaderboard[sender] = self.leaderboard[sender] + 1

                        # follow check
                        if self.is_following(sender, receiver) :
                            self.leaderboard[sender] = self.leaderboard[sender] + 1
                        
                        
                            
                                

                    # lurking command
                    if(x[2]) in message_text:
                        if self.existsInLurkList(sender, self.users[receiver]):
                            self.send_message(receiver, "@"+sender+" is already in our lurk list!")
                        else :
                            self.addToLurkList(sender, self.users[receiver])
                            self.send_message(receiver, "@"+sender+" has beend added in our lurk list!")
                    # follow command
                    if(x[3]) in message_text:
                        if self.existsInFollowList(sender, self.users[receiver]):
                            self.send_message(receiver, "@"+sender+" is already in our Follow list!")
                        else :
                            self.AddToFollowList(sender, self.users[receiver])
                            self.send_message(receiver, "@"+sender+" has beend added in our Follow list!")

            # user commands
            if receiver in self.user.commands:
                for x in self.user.commands[receiver]:
                    if x["command"] in message_text:
                        self.send_message(receiver, x["message"])
            
        


    def run(self):
        
        self.user.updateCommands()
        self.user.updateCustomCommands()
        self.user.updateTimers()
        self.user.updateUsers()
        self.user.updateTwitchToken()
        
        self.connect()
        for x in self.user.users:
            self.join_channel(x)


        leaderboard_update_thread = threading.Thread(target=self.update_Leaderboard, args=[])
        leaderboard_update_thread.start()
        
        
        
        
        while True:
            response = self.irc.recv(1024).decode('utf-8')
            print(response, flush=True)
            self.proccess_server_message(response)

        


app = App()
app.updateUsers()
app.run()
