from db.database import DB_Connect
import requests
import time

class User:

    id                  =   0
    commands            =   {}
    custom_commands     =   []
    timers              =   {}
    lurk_list           =   {}
    users               =   []
    prev_host           =   []
    smartboot           =   {}

    def __init__(self):
        self.db = DB_Connect()
        self.smartboot['url'] = "http://192.168.0.100/smartboot/"
        self.smartboot['username'] = "smartboot"
        self.smartboot['password'] = "kishavec10"
        self.smartboot['token'] = ""


    def getUsername(self, user_id):
        cursor = self.db.get().cursor()
        cursor.execute("SELECT username FROM username where user=%s" % (user_id))
        username = cursor.fetchall()[0][0]
        self.db.close()
        return username

    def updateCustomCommands(self):
        self.custom_commands.clear()
        cursor = self.db.get().cursor()
        query = "select username.username, host_command, lurk_command, follow_command, live_settings.user FROM live_settings "
        query += "INNER join username on username.user = live_settings.user where username.active=1"
        cursor.execute(query)
        results = cursor.fetchall()
        for x in results:
            self.custom_commands.append(x)
        self.db.close()
    
    def updateUsers(self):
        self.users.clear()
        cursor = self.db.get().cursor()
        query = "select username.username from username inner join user on user.id=username.user where user.active=1"
        cursor.execute(query)
        results = cursor.fetchall()
        self.db.close()
        for x in results:
            self.users.append(x[0])

    def updateTwitchToken(self):
        url = self.smartboot['url'] + "channel/token"
        r = requests.post(url, data={"username": self.smartboot['username'], "password" : self.smartboot['password']}).json()
        if(len(r) > 0 ):
            self.smartboot["token"] = r['token']

    def updateCommands(self):
        self.commands.clear()
        cursor = self.db.get().cursor()
        query =     "SELECT username.username,command.command,command.message,command.level,command.cooldown from command inner join user ON "
        query +=    "user.id = command.user INNER join username ON username.user = user.id where command.active=1"
        cursor.execute(query)
        results = cursor.fetchall()
        for x in results:
            if x[0] not in self.commands:
                self.commands[x[0]] = []
            d = {"command" : x[1], "message": x[2], "level": x[3], "cooldown" : x[4] }
            self.commands[x[0]].append(d)
        self.db.close()
    
    def updateTimers(self):
        self.timers.clear
        cursor = self.db.get().cursor()
        query =     "SELECT username.username,timer.timer,timer.message,timer.interval,timer.lines from timer inner join user on "
        query +=    "user.id = timer.user INNER join username ON username.user = user.id where timer.status=1"
        cursor.execute(query)
        results = cursor.fetchall()
        for x in results:
            if x[0] not in self.timers:
                self.timers[x[0]] = []
            d = {"timer" : x[1], "message": x[2], "interval": x[3], "lines" : x[4] }
            self.timers[x[0]].append(d)
        self.db.close()

    
        