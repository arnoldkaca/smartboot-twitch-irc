#!/usr/bin/python
import mysql.connector as mysql

class DB_Connect():
    connection = None
    db_host = "localhost"
    db_user = "root"
    db_pass = ""
    db_name = "smartboot"

    
    def set(self):
        self.connection = mysql.connect(host=self.db_host, user=self.db_user, password=self.db_pass, database=self.db_name)

    def get(self):
        self.set()
        return self.connection
    
    def commit(self):
        self.connection.commit()

    def close(self):
        self.connection.close()

